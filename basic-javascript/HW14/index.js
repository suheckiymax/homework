const firstScreen = document.documentElement.clientHeight;
const scrollTop = $('.scroll-up');
const hideBtn = $('.toggle-btn');

scrollTop.hide();

hideBtn.on('click', function (e) {
    e.preventDefault();
    $('.gallery').slideToggle();
});

$('.nav_list a').on('click', function (e) {
    e.preventDefault();
    $('html, body').animate({
        scrollTop: $($(this).attr('href')).offset().top
    }, 1000);
});

$(document).on('scroll', function () {
    if ($(this).scrollTop() > firstScreen * 2) scrollTop.show();
});

scrollTop.on('click', function () {
    $('html, body').animate({
        scrollTop: 0,
    }, 500, function () {
        scrollTop.hide();
    });
});


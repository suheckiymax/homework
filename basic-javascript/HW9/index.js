const tabs = document.querySelector('.tabs');

tabs.addEventListener('click', function (event) {
    let target = event.target;

    if (target.className === 'tabs-title') {
        dropActive();
        target.classList.add('active');
        getContent(target.dataset.content);
    }
});

function dropActive () {
    const tabsContent = document.querySelectorAll('.tabs-title');

    tabsContent.forEach(function (item) {
        item.classList.remove('active');
    });
}

function getContent (dataTab) {
    const content = document.querySelector('.tabs-content');

    [...content.children].forEach(function (item) {
        item.hidden = dataTab !== item.dataset.content;
    });
}


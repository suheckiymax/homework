function listOutput (arr, parent = document.body) {
    let list = `<ul>${arr.map((item) => `<li>${item}</li>`).join('')}</ul>`;
    parent.insertAdjacentHTML('afterbegin', list);
}

listOutput(["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"]);
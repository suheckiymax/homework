const picture = document.querySelectorAll('.image-to-show');
const terminateSlider = document.querySelector('.terminate');
const resumeSlider = document.querySelector('.resume');
let i = 0;

function slider (picture) {
    picture[i].hidden = 'false';
    i++;
    if (i === picture.length) i = 0;
    picture[i].hidden = !picture[i].hasAttribute('hidden');
}

terminateSlider.addEventListener('click',  () => clearInterval(timerId));
resumeSlider.addEventListener('click', function () {
    clearInterval(timerId);
    timerId = setInterval(slider, 3000, picture);
});

let timerId = setInterval(slider, 3000, picture);
let userName = prompt('What is your name?', 'Peter');
let userAge = +prompt('How old are you?', '30');

if (userAge < 18) {
    alert('You are not allowed to visit this website!');
} else if (userAge >= 18 && userAge <= 22) {
    let question = confirm('Are you sure you want to continue?');
    if (question) {
        alert(`Welcome ${userName}!`);
    } else {
        alert('You are not allowed to visit this website!');
    }
} else {
    alert(`Welcome ${userName}!`);
}
let student = {},
    i = 0,
    sumGrade = 0;

student.name = prompt("What is the student's name?", "George");
student.lastName = prompt("What is the student's surname?", "Wilson");
student.tabel = {};

while (true) {
    let subjectTitle = prompt("Enter the name of the subject");
    if (subjectTitle === null) break
    student.tabel[subjectTitle] = +prompt("Enter a grade for the subject");
}

for (let key in student.tabel) {
    if (student.tabel[key] < 4) i++;
    sumGrade += student.tabel[key];
}

let average = sumGrade / Object.keys(student.tabel).length;

if (i === 0) alert('Студент переведен на следующий курс.');
if (average > 7) alert('Студенту назначена стипендия.');
function createUser() {
    let name = prompt('What is your name?', 'George');
    let lastName = prompt('What is your last name?', 'Wilson');

    const newUser = {
        firstName: name,
        lastName: lastName,
        setFirstName(newFirstName) {
            Object.defineProperty(this, 'firstName', {
                value: newFirstName
            });
        },
        setLastName(newLastName) {
            Object.defineProperty(this, 'lastName', {
                value: newLastName
            });
        }
    }

    Object.defineProperties(newUser, {
        'firstName' : {
            configurable: false,
            writable: false
        },
        'lastName' : {
            configurable: false,
            writable: false
        }
    });

    newUser.setFirstName();
    newUser.setLastName();
}

createUser();






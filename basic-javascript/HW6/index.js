function filterBy(myArray, typeData) {
    return myArray.filter(e => typeof e !== typeData)
}


let result = filterBy([1, 2, 'abc', -10, 0, {name: 'Max'}, 'test'], 'number')

console.log(result);
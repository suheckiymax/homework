function factorial(userNumber) {
    if (+userNumber === 1) {
        return 1
    } else {
        return factorial(userNumber - 1) * userNumber
    }
}

let userNumber = prompt('Enter the number of the factorial!');

while (isNaN(+userNumber) || userNumber === '' || +userNumber === 0) {
    userNumber = prompt('Enter the number of the factorial!', '4');
}

alert(factorial(userNumber));
const style = document.querySelector('#stylesheet');
const themeBtn = document.querySelector('.color-theme-text');
const theme = localStorage.getItem('theme');

theme === null ? style.href = 'style/style-light.css' : style.href = theme;

themeBtn.addEventListener('click', () => {
    if (style.getAttribute('href') === 'style/style-light.css') {
        style.href = 'style/style-dark.css';
        localStorage.setItem('theme', 'style/style-dark.css');
    } else {
        style.href = 'style/style-light.css';
        localStorage.setItem('theme', 'style/style-light.css');
    }
});
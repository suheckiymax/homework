let userNumber = +prompt('Enter your number');

while (userNumber !== parseInt(userNumber, 10)) {
    userNumber = +prompt('Enter integer number', '10');
}

if (userNumber < 5) {
    console.log('Sorry, no numbers');
} else {
    for (let i = 0; i <= userNumber; i += 5) {
        console.log(`Multiple 5: ${i}`);
    }
}

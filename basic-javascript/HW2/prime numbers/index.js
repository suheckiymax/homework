let firstNumber = +prompt('Enter M number');
let secondNumber = +prompt('Enter N number');

while (firstNumber >= secondNumber) {
    alert('Error!');
    firstNumber = +prompt('Enter M number', '20');
    secondNumber = +prompt('Enter N number', '40');
}

nextPrime:
for (let i = firstNumber; i <= secondNumber; i++) {
    for (let j = 2; j < i; j++) {
        if (i % j === 0) continue nextPrime;
    }

    if (i !== 0 && i !== 1) {
        console.log(i);
    }
}

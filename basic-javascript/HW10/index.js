const iconPassword = document.getElementsByClassName('fas');
const password = document.getElementsByClassName('password');
const confirm = document.querySelector('.btn');

[].forEach.call(iconPassword, function (el) {
    el.addEventListener('click', function () {
        if (el.classList.contains('fa-eye')) {
            el.classList.replace('fa-eye','fa-eye-slash');
            el.previousElementSibling.setAttribute('type', 'text');
        } else {
            el.classList.replace('fa-eye-slash','fa-eye');
            el.previousElementSibling.setAttribute('type', 'password');
        }
    });
});

confirm.addEventListener('click', function () {
    if (password[0].value === password[1].value) {
        alert('You are welcome!');
    } else {
        const warning = document.createElement('p');
        warning.classList.add('warning');
        warning.textContent = 'You must enter the same values';
        password[1].after(warning);
    }
});
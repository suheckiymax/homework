function createNewUser () {
    return  {
        firstName: prompt('What is your name?', 'George'),
        lastName: prompt('What is your last name?', 'Wilson'),
        birthday: prompt('Enter your date of birth', 'dd.mm.yyyy'),
        getLogin() {
            return (this.firstName[0] + this.lastName).toLowerCase();
        },
        getAge() {
            let dateArray = this.birthday.split('.'),
                date = dateArray[1] + '.' + dateArray[0] + '.' + dateArray[2];

            return (new Date().getTime() - new Date(date)) / (24 * 3600 * 365.25 * 1000) | 0;
        },
        getPassword() {
            let birthdayYear = this.birthday.split('.');
            return (this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + birthdayYear[2]);
        }
    }
}

const newUser = createNewUser();

console.log(`Login: ${newUser.getLogin()};\nPassword: ${newUser.getPassword()};\nUser age: ${newUser.getAge()}`);
const clear = document.createElement('span');
const error = document.createElement('p');
const currentPrice = document.createElement('p');
const priceText = document.querySelector('.price-text');
const price = document.querySelector('.price');

clear.textContent = ' X ';
clear.classList.add('clear');
error.classList.add('error-text');

price.addEventListener('focus', () => {
    price.classList.add('price-focus');
    if (price.classList.contains('price-blur')) {
        error.remove();
        price.classList.remove('price-blur');
    }
});

price.addEventListener('blur', () => {
    price.classList.remove('price-focus');
    if (price.value > 0) {
        currentPrice.textContent = `Current price: ${price.value}`;
        priceText.prepend(currentPrice);
        currentPrice.append(clear);
        price.classList.add('price-focus');
    } else if (price.value === '') {
        error.remove();
        price.classList.remove('price-blur');
    } else {
        error.textContent = 'Please enter correct price';
        price.classList.add('price-blur');
        price.after(error);
    }
});

clear.addEventListener('click', () => {
    price.value = '';
    currentPrice.remove();
    price.classList.remove('price-focus');
});

let firstNumber;
let secondNumber;

do {
    firstNumber = prompt('Enter first number!', '2');
    secondNumber = prompt('Enter second number!','2');
} while (isNaN(+firstNumber && +secondNumber) || firstNumber === '' || secondNumber === '');

firstNumber = +firstNumber;
secondNumber = +secondNumber;

let mathOperation = prompt('Enter mathematical operation', '+');

function calc(firstNumber, secondNumber, mathOperation) {
    switch (mathOperation) {
        case '+':
            return firstNumber + secondNumber;
        case '-':
            return firstNumber - secondNumber;
        case '/':
            return firstNumber / secondNumber;
        case '*':
            return firstNumber * secondNumber;
        default:
            alert('Error!');
    }
}

let result = calc(firstNumber, secondNumber, mathOperation);

console.log(result);
import React from 'react';
import ReactDOM from 'react-dom';
import './index.scss';
import App from './App';
import Header from "./components/Header/Header";
import reportWebVitals from './reportWebVitals';
import {BrowserRouter} from "react-router-dom";

ReactDOM.render(
  <BrowserRouter>
      <Header/>
      <App/>
  </BrowserRouter>,
  document.getElementById('root')
);

reportWebVitals();

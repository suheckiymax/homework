import React, { Component } from 'react';
import PropTypes, {object} from 'prop-types'
import Product from "../Product/Product";
import './ProductList.scss'
export default class ProductList extends Component {
    render() {
        const {list, addToFavorite, handleClick} = this.props;

        const productsList = list.map(product =>
            <Product key={product.code}
                     code={product.code}
                     name={product.name}
                     price={product.price}
                     picture={product.picture}
                     color={product.color}
                     isFavorite={product.isFavorite}
                     product={product}
                     addToFavorite={addToFavorite}
                     handleClick={handleClick} />
        )

        return (
            <div className='products-container'>
                {productsList}
            </div>
        )
    }
}

ProductList.propTypes = {
    list: PropTypes.arrayOf(object).isRequired,
    addToFavorite: PropTypes.func.isRequired,
    handleClick: PropTypes.func.isRequired
}
import React, { Component } from 'react';
import PropTypes from 'prop-types'
import './Product.scss'
import { favorite } from "../../utils/index";
import Button from "../Button/Button";

export default class Product extends Component {
    render() {
        const {product, picture, name, color, price, handleClick, addToFavorite, isFavorite, code} = this.props;

        return (
            <div className='product'>
                <div className='product__header'>
                    <p className='product__header-code'>Code: {code}</p>
                    <button className='product__header-btn' onClick={() => addToFavorite(code)}>
                        {isFavorite ? favorite('1', 'product__header-favorite') : favorite('0', 'product__header-favorite')}
                    </button>
                </div>
                <div className='product__about'>
                    <a href="#">
                        <img src={picture} alt={name} className='product__header-img'/>
                    </a>
                    <p className='product__about-title'>{name} <br/> ({color})</p>
                </div>
                <div className='product__control'>
                    <p className='product__control-price'>{price}</p>
                    <Button classes="product__control-btn"
                            text='Add to cart'
                            handleClick={() => handleClick('add', product)}/>
                </div>
            </div>
        )
    }
}

Product.propTypes = {
    picture: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    color: PropTypes.string,
    price: PropTypes.string.isRequired,
    handleClick: PropTypes.func.isRequired,
    addToFavorite: PropTypes.func.isRequired,
    code: PropTypes.string.isRequired
}

Product.defaultProps = {
    color: ''
}
import React from 'react'
import modalWindowDeclarations from "./modalWindows";
import Button from './components/button/Button'
import Modal from "./components/modal/Modal";
import './App.scss'

export default class App extends React.Component {
    state = {
        status: '',
        modal: ''
    }

    closeModal()  {
        this.setState({
            status: '',
            modal: ''
        })
    }

    showModal() {
        return <Modal
            id={this.state.modal.id}
            title={this.state.modal.title}
            desc={this.state.modal.desc}
            closeModal={() => this.closeModal()}
            closeButton={this.state.modal.closeButton}
            actions={{
                ok: <button className='modal__control-btn' onClick={() => this.closeModal()}>{this.state.modal.confirm}</button>,
                cancel: <button className='modal__control-btn' onClick={() => this.closeModal()}>{this.state.modal.cancel}</button>
            }} />
    }

    modal(e) {
        const modalID = e.target.id;
        const modalDeclaration = modalWindowDeclarations.find(item => item.id === modalID);

        this.setState({
            status: modalID,
            modal: modalDeclaration
        })
    }

    render() {
        return (
            <>
                <div className='btn-container'>
                    <Button classes='btn btn--danger' text='Open first modal' id='modalID1' handleClick={(e) => this.modal(e)} />
                    <Button classes='btn btn--success' text='Open second modal' id='modalID2' handleClick={(e) => this.modal(e)} />
                </div>
                {this.state.status && this.showModal()}
            </>
        );
    }
}
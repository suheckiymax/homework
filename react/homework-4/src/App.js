import './App.scss';
import React, {useEffect} from 'react';
import Header from "./components/Header/Header";
import Footer from "./components/Footer/Footer";
import Home from "./pages/Home/Home";
import Cart from "./pages/Cart/Cart";
import Favorites from "./pages/Favorites/Favorites";
import Page404 from "./pages/NotFound/Page404";
import {BrowserRouter as Router, Route, Switch} from "react-router-dom";
import {useDispatch} from "react-redux";
import {getDataThunk} from "./redux/getData/actions";

const App = () => {
    const dispatch = useDispatch()

    useEffect(() => {
        dispatch(getDataThunk())
    }, [dispatch])

    return (
        <Router>
            <Header/>
                <main className='main'>
                    <Switch>
                        <Route exact path='/' component={Home}/>
                        <Route path='/cart' component={Cart}/>
                        <Route path='/favorites' component={Favorites}/>
                        <Route component={Page404}/>
                    </Switch>
                </main>
            <Footer/>
        </Router>
    )
}

export default App
import React from 'react';
import { NavLink } from "react-router-dom";
import './Header.scss'

const Header = () => {
    return (
        <header className='header'>
            <div className='header__logo'>

            </div>
            <div className='header__nav'>
                <ul className='nav'>
                    <li className='nav__item'>
                        <NavLink exact to='/' className='nav__item-link'
                                 activeClassName='nav__item-link--selected'>Home</NavLink>
                    </li>
                    <li className='nav__item'>
                        <NavLink to='/cart' className='nav__item-link'
                                 activeClassName='nav__item-link--selected'>Cart</NavLink>
                    </li>
                    <li className='nav__item'>
                        <NavLink to='/favorites' className='nav__item-link'
                                 activeClassName='nav__item-link--selected'>Favorites</NavLink>
                    </li>
                </ul>
            </div>
        </header>
    )
}

export default Header
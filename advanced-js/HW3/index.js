class Employee {
    constructor(name, age, salary) {
        this._name = name;
        this._age = age;
        this._salary = salary;
    }

    get name () {
        return this._name
    }

    set name (value) {
        this._name = value;
    }

    get age () {
        return this._age;
    }

    set age (value) {
        this._age = value;
    }

    get salary () {
        return this._salary
    }

    set salary (value) {
        this._salary = value
    }
}

class Programmer extends Employee {
    constructor(name, age, salary, lang) {
        super(name, age, salary);
        this.lang = lang;
    }

    get salary () {
       return `$${this._salary * 3}`
    }

    set salary (newSalary) {
        this._salary = newSalary
    }
}

const employee = new Employee('George', 28, 400);
const programmer1 = new Programmer('Andrew', 30, 1000, ['Spanish', 'Chinese']);
const programmer2 = new Programmer('Jhon', 23, 700, ['Dutch', 'French']);

console.log(programmer1, `New salary: ${programmer1.salary}`, programmer2, `New salary: ${programmer2.salary}`);
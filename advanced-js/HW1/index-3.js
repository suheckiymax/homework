const user1 = {
    name: "John",
    years: 30
};

const {name, years: age, isAdmin = false} = user1;

document.write(`${name} ${age} ${isAdmin}`);
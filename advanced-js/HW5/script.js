class LocationByIP {
    constructor() {
        this.data = this.getData();
        this.elements = {
            btn: document.createElement('button'),
            p: document.createElement('p')
        }
    }

    async getData() {
        const ip = await fetch('https://api.ipify.org/?format=json');
        const jsonIP = await ip.json();
        const data = await fetch(`http://ip-api.com/json/${jsonIP.ip}?fields=continent,country,regionName,city,district`);
        return data.json();
    }

    async render() {
        const {btn, p} = this.elements;
        const {continent, country, regionName, city, district} = await this.data;
        btn.textContent = 'Вычислить по IP';
        p.textContent = 'Местоположение: ';
        btn.classList.add('btn');
        document.body.prepend(btn);

        p.append(`Континент - ${continent}, Страна - ${country}, Регион - ${regionName}, Город - ${city}, Район - ${district}`);

        btn.addEventListener('click', () => btn.after(p));
    }
}

const locationByIP = new LocationByIP();

locationByIP.render();


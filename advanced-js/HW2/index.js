const books = [
    {
        author: "Скотт Бэккер",
        name: "Тьма, что приходит прежде",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Воин-пророк",
    },
    {
        name: "Тысячекратная мысль",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Нечестивый Консульт",
        price: 70
    },
    {
        author: "Дарья Донцова",
        name: "Детектив на диете",
        price: 40,
    },
    {
        author: "Дарья Донцова",
        name: "Дед Снегур и Морозочка",
    }
];

const container = document.getElementById('root');
const list = document.createElement('ul');
container.append(list);

function showList ({author, name, price}) {
    if (author && name && price) {
        list.insertAdjacentHTML('afterbegin', `<li>${author}, ${name}, ${price}</li>`);
    } else {
        if (!author) {
            throw new Error(`Missing property author`);
        } else if (!name) {
            throw new Error(`Missing property name`);
        } else if (!price) {
            throw new Error(`Missing property price`);
        }
    }
}

books.forEach(({author, name, price}) => {
    try {
        showList({author, name, price});
    } catch (e) {
        console.error(e.message);
    }
});
